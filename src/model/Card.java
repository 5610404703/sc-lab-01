package model;

public class Card {
	private double balance;
	private double money;
	private String ans;
	
	public Card(double balance){
		this.balance = balance;
	}
	 
	public double getBalance(){
		return balance;		 
	}
 
	public double refillMoney(double money){
		ans = " " + money;
		balance += money;
		return balance;		 
	}

	public double buy(double money){
		ans = " " + money;
		balance -= money;
		return balance;		 
	}
	 
	public String toStringBalance() {
		return   " �ʹ�Թ������� " +balance;
	}
	 
	public String toStringRefill() {
		return   " ����Թ " +ans;
	}
	
	public String toStringPay() {
		return   " �ѡ�Թ " +ans;
	}

}
