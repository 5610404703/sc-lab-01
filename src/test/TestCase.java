package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import GUI.Frame;
import test.TestCase.ListenerMgr;
import model.Card;

public class TestCase {
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		new TestCase();
	}

	public TestCase() {
		frame = new Frame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(700, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		
		Card card = new Card(0);
		card.getBalance();
		frame.setResult(card.toStringBalance());
		card.refillMoney(500);
		frame.extendResult(card.toStringRefill());
		frame.extendResult(card.toStringBalance());
		card.buy(150);
		frame.extendResult(card.toStringPay());
		frame.extendResult(card.toStringBalance());
		card.refillMoney(200);
		frame.extendResult(card.toStringRefill());
		frame.extendResult(card.toStringBalance());
		card.buy(400);
		frame.extendResult(card.toStringPay());
		frame.extendResult(card.toStringBalance());
		
	}

	ActionListener list;
	Frame frame;
}



